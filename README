Salut à Toi
(c) Jérôme Poisson aka Goffi 2008-2021
(c) Adrien Cossa aka Souliane 2013-2016

SàT is a XMPP (Jabber) client.


** LICENCE **

SàT is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SàT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with SàT.  If not, see <http://www.gnu.org/licenses/>.


** ABOUT **

SàT is a XMPP (Jabber) client, made on a daemon/frontends architecture. Its aim is not only to be an instant messaging client: SàT manage microblogging, file transfer, rich text edition, piping over XMPP, XMPP remote, etc.
In addition, SàT has been built with ethic in mind, a social contract show the main lines (see CONTRAT_SOCIAL or CONTRAT_SOCIAL_en). We have a lot of debates about the right way to do things, more on a social way than on a technical way (but consider technique too).

For the moment, the frontends are:

* primitivus: a console interface client, for terminal lovers
* jp: the powerful command line toolkit.
* libervia: the web frontend. It's in a different package, check libervia on the wiki: https://wiki.goffi.org/wiki/Libervia
* cagou: the desktop/mobile frontend. It's in a different package
* bellaciao: a Qt frontend at an early development stage (development currently paused)


** HOW TO USE IT ? **

For installation instructions, please read the file "INSTALL". Note that SàT is available in several GNU/Linux distributions

A wiki is available online (https://wiki.goffi.org), in French and English so far (we would appreciate any help for other languages). You can also have a look at the bottom of this files (in contact section) for other available websites.


** MISC **

To use SàT, you need to have D-Bus daemon launched (http://www.freedesktop.org/wiki/Software/dbus/). If you use X Window, it should already be launched. If your are on a terminal environment without X11 (e.g. on a server) you may have to launch it. You can launch a daemon using the following command:
$ eval `dbus-launch --sh-syntax`

Be careful to use the same environment variables if you start a new session.

I personally use the following script to do that automatically:

--- /usr/local/bin/dbus-launch.sh ---
#!/bin/sh

DBUS_PATH="/tmp/.dbus.`whoami`"

if [ ! -e $DBUS_PATH ]; then
        dbus-launch --sh-syntax > $DBUS_PATH
        chmod 400 $DBUS_PATH
fi

cat $DBUS_PATH
--- end of /usr/local/bin/dbus-launch.sh ---

You can launch this script by putting at the end of your .zshrc (or whatever you're using):
eval `/usr/local/bin/dbus-launch.sh`

--

You can find Primitivus shortcuts on the wiki:
https://wiki.goffi.org/wiki/Primitivus

--

SàT is the acronym (yes another one :( ) in tribute to the song Salut à Toi from the Bérurier Noir band and it means "hi to you" (which seems appropriate for a communication software). If you want to listen to the song, we also recommend the excellent cover from Les Ogres de Barback.
jp stands for "Jabber coPy" or "JumP", and was choosen for its similarity with "cp" and short name.
Primitivus is based on Urwid, and, according to their FAQ, "ur" is a German prefix for "ancestral or primal" (primitivus is a latin word).
Libervia is made from "Liber" (libre) and via (road, path). It could be understood as the libre road, or the path to freedom.
Cagou is the name of a beautiful bird (who bark and can't fly) from New-Caledonia, and it's also a wink to Kivy, the framework used.


** CREDIT **

A big big thank to the authors/contributors of...

proxy65:
SàT (plugin_xep_0065) use nearly all the code from proxy65 (http://code.google.com/p/proxy65/) which was coded by Dave Smith (2002-2004) and maintained by Fabio Forno (2007-2008).
As the original MIT licence allows, the code is reused and sub-licenced until GPL v3 to follow the rest of the code.

progressbar:
SàT (jp) use ProgressBar (http://pypi.python.org/pypi/progressbar/2.2), a class coded by Nilton Volpato which allow the textual representation of progression.

twisted:
SàT is heavily based on the twisted framework (http://twistedmatrix.com/trac/), a very great tool which offer a lot of protocols management. There are too many contributors to name them here, so take a look on the website :).

wokkel:
SàT use a library with high level enhancements on top of twisted, which is called wokkel (http://wokkel.ik.nu). Lot of thanks to Ralph Meijer and all other contributors.

Urwid:
Primitivus is based on Urwid (http://excess.org/urwid/) which saved me a lot of time. It's really a great library to easily make a sophisticated interface.

Pyjamas:
Libervia is built with a Pyjamas (http://pyjs.org), a Google Web Toolkit port for python, including Python to Javascript compiler, and Pyjamas Desktop which allow to execute the same application on the desktop or through a browser. It's really an amazing tool.

Kivy and its linked tools (python-for-android, buildozer, plyer, pyjnius):
Kivy and linked tools (https://kivy.org) are used to build Cagou frontend, and to port it on several platforms. Excellent pieces of software, well thought, 

Kivy garden:
in addition to Kivy itself, extension from the garden are used:
- contextmenu: used to display main and context menus

lxml(http://lxml.de/):
this powerful and efficient XML parsing module is used sometimes to replace Twisted internal tools: its API is handy, and it have some features like evil content cleaning.

pillow(https://python-pillow.github.io/):
This image manipulation module is used for avatars

txJSON-RPC:
Libervia use txJSON-RPC (https://launchpad.net/txjsonrpc), a twisted library to communicate with the browser's javascript throught JSON-RPC

Mutagen:
Mutagen (https://bitbucket.org/lazka/mutagen) is an audio metadata handling library, it's used by the radiocol plugin.

Python OTR (http://python-otr.pentabarf.de), PyCrypto (https://www.dlitz.net/software/pycrypto) and pyOpenSSL(https://github.com/pyca/pyopenssl):
Used for cryptography

otr.js and its dependencies Big Integer Library, CryptoJS, EventEmitter:
Libervia frontend uses otr.js and its dependencies:
    - otr.js was coded by Arlo Breault (2014) and is released under the Mozilla Public License Version 2.0
    - Big Integer Library was coded by Leemon Baird (2000-2013) and is in the public domain
    - CryptoJS was coded by Jeff Mott (2009-2013) and is released under the MIT licence
    - EventEmitter was coded by Oliver Caldwell (2011-2013) and is released under the MIT licence
As the original licences allow, the code is reused and sub-licenced until GPL v3 to follow the rest of the code.

mardown (https://pythonhosted.org/Markdown/) and html2text (https://pypi.python.org/pypi/html2text/2015.6.21):
both are used for syntaxes conversions

Jinja2 (http://jinja.pocoo.org/):
a poweful template engine for Python that we use for designing Libervia's static blog pages

miniupnp (http://miniupnp.free.fr/):
this UPnP-IGD implementation is used to facilitate P2P sessions

netifaces (https://pypi.python.org/pypi/netifaces):
when available, this module is used to detect local IPs

pictures found in the sat_media repository and used by SàT and Libervia:
Please read the credits and licence information that are given in the README and COPYING files for each work: http://repos.goffi.org/sat_media/file

the powerfull ImageMagick (http://www.imagemagick.org/) is used by the script written to split the previously named picture.

PyXDF (http://freedesktop.org/wiki/Software/pyxdg):
Used to follow FreeDesktop XDG standards

A special thank to people working on XMPP standards, libre standards are the way to go !

and the others:
and of course, nothing would be possible without Python (http://www.python.org/), GNU and the Free Software Foundation (http://www.gnu.org, http://www.fsf.org/), the Linux Kernel (http://www.kernel.org/), and the coder of the tools we use like Vim (http://www.vim.org/), Mercurial (http://www.selenic.com/mercurial/wiki/), or all the KDE stuff (http://www.kde.org/ and of course http://amarok.kde.org/), and also XFCE (http://www.xfce.org), etc. Thanks thanks thanks, thanks to everybody in the Free (Libre) chain for making a part of this.

If we forgot any credit (and we probably have), please contact us (mail below) to fix it.


** CONTRIBUTORS **

Salut à Toi has received contributions from:

- Adrien Vigneron <adrienvigneron@mailoo.org>: huge work on Libervia's CSS, SàT Logo (the mascot is his work), and Quiz game graphics.

- Xavier Maillard <xavier@maillard.im>: bugs fixes, sat_templates installation.

- Emmanuel Gil Peyrot <linkmauve@linkmauve.fr>: bugs fixes, Libervia's notification, Libervia as a twisted application plugin.

- Matthieu Rakotojaona <matthieu.rakotojaona@gmail.com>: English translation of the social contract.

- Thomas Preud'homme <robotux@debian.org>: bugs fixes. He's also one of the co-maintainer of the Debian package.

- Dal <kedals0@gmail.com>: profiles management, argparse refactoring in jp.

- Matteo Cypriani <mcy@lm7.fr>: jp's mainloop update + doc improvements + various fixes. He's also the other co-maintainer of the Debian package.

- Olly Betts <olly@survex.com>: icon fix in Wix [N.B.: Wix has since been removed]

- Geoffrey Pouzet <chteufleur@kingpenguin.tk>: XEP-0070 and XEP-0184 implementations

- Arnaud Joset <info@agayon.be>: setup fixes

Many thanks to them.

A big thank also to all the maintainers of SàT packages.


** CONTRIBUTIONS **

Here are the URIs you can use to publish/retrieve tickets or merge requests:

tickets: xmpp:pubsub.goffi.org?;node=org.salut-a-toi.tickets%3A0 (please use "core" label)
merge requests: xmpp:pubsub.goffi.org?;node=org.salut-a-toi.merge_requests%3A0 (please use "core" label)

Tickets and merge requests are handled by SàT itself using XMPP.


** CONTACT **

You can join us on the XMPP MUC room (sat@chat.jabberfr.org), or individually:

SàT team:
	- contact@salut-a-toi.invalid (replace invalid by org)
	- https://libervia.org/blog/salut-a-toi (blog)

Goffi:
	- goffi@goffi.org (e-mail)
	- goffi@jabber.fr (jid)
	- https://www.goffi.org (blog - based on Libervia -, with fresh news about SàT)

Souliane:
	- souliane@mailoo.org (e-mail)
	- souliane@libervia.org (jid)

You'll find the latest version and other stuffs on *.goffi.org websites:

- wiki (wiki.goffi.org), in French & English so far
- ftp (ftp.goffi.org) for the latest version, or previous ones (and other projects)
- bugtracker (bugs.goffi.org) to report any problem or give suggestions
- mailing lists (lists.goffi.org)

and the official SàT website is at https://www.salut-a-toi.org


This software is dedicated to Roger Poisson.
