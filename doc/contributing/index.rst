.. _contributing:

============
contributing
============

This part of the documentation is for people willing to contribute to the development of
Libervia and its ecosystem, it is not needed to read this if you only plan to use the
software.

.. toctree::
   :glob:
   :maxdepth: 2

   testing
