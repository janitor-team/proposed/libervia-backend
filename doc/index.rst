======================
Libervia documentation
======================

Welcome to Libervia's documentation. You'll find here both end-user and developer documentations.

Libervia is a Libre communication ecosystem based on XMPP standard. It allows you to do many things such as:

- instant messaging
- (micro)blogging
- file sharing
- managing photo albums
- organizing/managing events
- handling tasks
- etc.

It features many interfaces (desktop, mobile, web, command line, console), and is multi-platforms.

You can follow this documentation to learn more on it, or join our official XMPP room at `sat@chat.jabberfr.org <xmpp:sat@chat.jabberfr.org?join>`_ (also available via a `web link <https://chat.jabberfr.org/converse.js/sat@chat.jabberfr.org>`_)


.. toctree::
   :caption: Contents:
   :maxdepth: 3

   installation.rst
   overview.rst
   configuration.rst
   components.rst
   /libervia-cli/index.rst
   /libervia-tui/index.rst
   /contributing/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
