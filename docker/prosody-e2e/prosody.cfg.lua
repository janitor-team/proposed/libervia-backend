-- Prosody XMPP Server Configuration
-- Adapted for SàT e2e tests

local socket = require "socket"

daemonize = false;
admins = { }

plugin_paths = { "/usr/local/share/prosody/modules" }

modules_enabled = {
	"admin_adhoc";
	"blocklist";
	"carbons";
	"csi";
	"csi_simple";
	"delegation";
	"dialback";
	"disco";
	"mam";
	"pep";
	"ping";
	"private";
	"privilege";
	"register";
	"roster";
	"saslauth";
	"smacks";
	"time";
	"tls";
	"uptime";
	"vcard4";
	"vcard_legacy";
	"version";
	"ipcheck";
}

modules_disabled = {
}

allow_registration = true
registration_whitelist = { socket.dns.toip("backend") }
whitelist_registration_only = true

c2s_require_encryption = true
s2s_require_encryption = true
s2s_secure_auth = false

pidfile = "/var/run/prosody/prosody.pid"

authentication = "internal_hashed"

archive_expires_after = "1d"

log = {
    {levels = {min = "info"}, to = "console"};
}

certificates = "certs"

ssl = {
	key = "/usr/share/libervia/certificates/server1.test-key.pem";
	certificate = "/usr/share/libervia/certificates/server1.test.pem";
}

component_interface = "*"

VirtualHost "server1.test"
	privileged_entities = {
		["pubsub.server1.test"] = {
			roster = "get";
            message = "outgoing";
			presence = "roster";
		},
	}

	delegations = {
		["urn:xmpp:mam:2"] = {
			filtering = {"node"};
			jid = "pubsub.server1.test";
		},
		["http://jabber.org/protocol/pubsub"] = {
			jid = "pubsub.server1.test";
		},
		["http://jabber.org/protocol/pubsub#owner"] = {
			jid = "pubsub.server1.test";
		},
		["https://salut-a-toi/protocol/schema:0"] = {
			jid = "pubsub.server1.test";
		},
		["http://jabber.org/protocol/disco#items:*"] = {
			jid = "pubsub.server1.test";
		},
		["https://salut-a-toi.org/spec/pubsub_admin:0"] = {
			jid = "pubsub.server1.test";
		},
	}

VirtualHost "server2.test"

VirtualHost "server3.test"

-- Component "muc.server1.test" "muc"
-- 	modules_enabled = {
-- 		"muc_mam";
-- 		"vcard";
-- 	}

Component "pubsub.server1.test"
	component_secret = "test_e2e"
	modules_enabled = {"privilege", "delegation"}

Component "proxy.server1.test" "proxy65"

Component "files.server1.test"
	component_secret = "test_e2e"
