ARG REVISION
FROM libervia/backend:${REVISION:-dev}

LABEL maintainer="Goffi <tmp_dockerfiles@goffi.org>"

ARG REVISION
ARG DEBIAN_FRONTEND=noninteractive

USER root

RUN apt-get install -y --no-install-recommends yarnpkg
WORKDIR /home/libervia
USER libervia
RUN cd /src && hg clone https://repos.goffi.org/libervia-web -u "${REVISION:-@}" && \
    ~/libervia_env/bin/pip install -e libervia-web && \
    mv libervia-web/libervia_web.egg-info ~/libervia_env/lib/python3.*/site-packages

RUN ./entrypoint.sh \
    # we build here to avoid re-downloading node modules or other browser
    # dependencies on each run
    libervia-web fg -- --build-only && \
    libervia-backend stop

EXPOSE 8080 8443

ENTRYPOINT ["libervia-web"]
CMD ["fg"]
