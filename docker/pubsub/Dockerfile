FROM debian:bullseye-slim

LABEL maintainer="Goffi <tmp_dockerfiles@goffi.org>"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends locales python3-dev python3-venv python3-wheel mercurial libpq-dev gcc gnupg && \
    # we install postgresql repository to have latest version
    echo "deb http://apt.postgresql.org/pub/repos/apt buster-pgdg main" > /etc/apt/sources.list.d/pgdg.list && \
    python3 -c 'from urllib.request import urlopen; print(urlopen("https://www.postgresql.org/media/keys/ACCC4CF8.asc").read().decode())' | apt-key add - && \
    # now we can install the client
    apt-get install -y --no-install-recommends postgresql-client && \
    # it's better to have a dedicated user
    useradd -m libervia && \
    mkdir /src && chown libervia:libervia /src && \
    # we need UTF-8 locale
    sed -i "s/# en_US.UTF-8/en_US.UTF-8/" /etc/locale.gen && locale-gen

ENV LC_ALL en_US.UTF-8

WORKDIR /home/libervia
COPY entrypoint.sh /home/libervia
RUN chown libervia:libervia /home/libervia/entrypoint.sh && chmod 0555 /home/libervia/entrypoint.sh

USER libervia
RUN python3 -m venv libervia_env && libervia_env/bin/pip install -U pip wheel && cd /src && \
    # we install thoses packages in editable mode, so we can replace them easily with volumes
    hg clone https://repos.goffi.org/sat_tmp && ~/libervia_env/bin/pip install -e sat_tmp && \
    mv sat_tmp/sat_tmp.egg-info ~/libervia_env/lib/python3.*/site-packages && \
    hg clone https://repos.goffi.org/sat_pubsub && ~/libervia_env/bin/pip install -e sat_pubsub && \
    mv sat_pubsub/sat_pubsub.egg-info ~/libervia_env/lib/python3.*/site-packages

ENTRYPOINT ["/home/libervia/entrypoint.sh"]
