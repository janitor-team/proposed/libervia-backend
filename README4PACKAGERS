First of all, thank you for packaging SàT :)


############
Dependencies
############

SàT requires Python 2.7.
You will find below the list of Python modules the backend and the frontends depend on, as well as additional information.

------------------------------
Dependencies for SàT's backend
------------------------------

dbus
lxml >= 3.1.0
Mutagen
pillow
PyCrypto >= 2.6.1
PyOpenSSL
python-dbus
Python OTR
Twisted Core
Twisted Mail
Twisted Web
Twisted Words
Wokkel >= 0.7.1
XDG
zope.interface

Recommended: Mutagen, markdown, html2text, netifaces, miniupnp, service-identity

--------------------------------
Dependencies for the Jp frontend
--------------------------------

PyGI (PyGObject 3)

Recommended: progressbar, inotify, lxml

----------------------------------------
Dependencies for the Primitivus frontend
----------------------------------------

Urwid >= 1.2.0
Urwid-satext >= 0.6.1
GObject 2 (not imported directly in Primitivus, but needed for Urwid's main loop)
DBus

Suggested: Python X Library (for user notifications in a graphical session)

--------------------------------------
Dependencies for the Libervia frontend
--------------------------------------

Jinja2 (for static blogs only)
pyjamas (for building only)
txJSON-RPC

Recommended: pyOpenSSL


##################
Satellite projects
##################

Several project have been made around SàT, they are all available at http://repos.goffi.org:

	- Libervia: the both Ajax and static web frontend.

	- SàT PubSub: a PubSub service which manage experimental features like fine permission tuning.

	- SàT media: Media needed by the frontends.

	- Urwid SàText: Widgets for urwid library, designed for SàT but can be useful for other projects. Mandatory for Primitivus.

The following ones are not needed in a distribution, because they are in a too early stage of development, or not useful for general purpose:

	- Bellaciao: a Qt frontend, at an early stage of development.

	- Salut: an XMPP directory, at a very early stage of development.

	- SàT Website: the code of the website.


########
Licences
########

We are careful about the licences we use. Our projects are licenced as follow:

	- Libervia, SàT PubSub, Bellaciao, Salut, SàT Website: AGPL v3+

	- Urwid SàText: LGPL v3+

When an external code is included (portion or library), we indicate the source and the licence, and we are careful to take Libre licences only. If we made a mistake somewhere, please let us know.

The case of SàT Media is particular are there is a compilation of media from severals sources. SàT Logo was made by Adrien Vigneron and is under CC By-SA. Other works are all under Free licences, licences are specified in the base directory of each package.


#####################
Packages and versions
#####################

We have packaged Primitivus and Jp with the core backend, as we think theses frontends should be shipped with each SàT distribution.

Libervia is distributed as a separate package because of its specificities (it's a huge project by itself, it uses Pyjamas Python => JS Compiler, etc.). But it's part of the same project.

All the official frontends have the same version number, which is the version of the backend they use.


#######
Updates
#######

There is a SQLite database per user (default in ~/local/share/sat/.sat.db); it is updated automatically if there is a schema change.


##############
Contrat social
##############

The social contract is an important part of the project, please don't forget to distribute it along with SàT.


#############
Miscellaneous
#############

There is a completion file for Zsh which detects the parameters of Jp. It's available in the misc/ directory (.jp) and must be available in a path included in user's fpath.

The translations are global to all the frontends (no translations available in Libervia yet). There are not up-to-date since 0.4 version.

The .service file in misc repository is used for auto-launch feature of D-Bus (launching a frontend will automatically launch the backend if it's not already present), setup.py adapt the path once installed. If you put it yourself, be sure that the "Exec=" line link to the sat.sh script.
This file should be in .services dir, usually /usr/share/dbus-1/services/


Thank you again for you help; don't forget to give us your name and contact email so we can credit you, and don't hesitate to contact us if you have any question (on the sat XMPP room at sat@chat.jabberfr.org, or check README for individual contacts).
